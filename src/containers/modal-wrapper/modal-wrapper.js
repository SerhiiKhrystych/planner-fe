import React, { Component } from 'react';

import './modal-wrapper.css'

class ModalWrapper extends Component {

	render() {
		const { visible, children, close, title } = this.props;
		return visible ?
			<div className="app-modal-wrapper">
				<div className="app-modal">
					<div className="app-modal--header">
						<div className="title">{title}</div>
						<span className="close-modal" onClick={close}>&#10006;</span>
					</div>
					{ children }
				</div>
			</div>
			: null
	}
}

export default ModalWrapper;