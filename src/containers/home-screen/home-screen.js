import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

import './home-screen.css';
// import io from "../../service/io";

class HomePage extends Component {

	componentDidMount() {
		// const socket = io();

		// socket.on('/', () => {
		// 	console.log('Socket connected');
		// });
	}

	render() {
		return (
			<div className="app-home-screen">
				<h3>Home screen</h3>

				<NavLink className="link" to="/events">Events</NavLink>
				<NavLink className="link" to="/notes">Notes</NavLink>
				<NavLink className="link" to="/settings">Settings</NavLink>
			</div>
		)
	}
}

export default HomePage;