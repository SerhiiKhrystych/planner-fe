/* eslint-disable import/first */
import React, {Component} from 'react';
import { Formik, Form, Field } from 'formik';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';

import CardContent from '@material-ui/core/CardContent';

import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { selectUser } from '../../redux/selectors/auth';
import { updateUserProfile } from '../../redux/actions/auth';

import '../../form.css';
import '../../App.css';

import { FORMS } from "../../utils/forms";

class SettingsScreen extends Component {
	render() {
		const { updateUserProfile, userProfile } = this.props;
		return (
			<div className="app-main-content-container">
				<div className="form-wrapper">
					{
						!!userProfile ?
						<Formik
							initialValues={{ firstName: userProfile.firstName, lastName: userProfile.lastName, email: userProfile.email }}
							validationSchema={FORMS.settings.validationSchema}
							onSubmit={(values) => {
								updateUserProfile({ ...userProfile, ...values });
							}}
							render={(formikBag) => (
								<Card className="form">
									<CardContent>
										<Form>
											<Typography variant="h6" align="center" color="primary" >
												{FORMS.settings.formLabel}
											</Typography>
											{
												FORMS.settings.fields.map((configs, key) => (
													<Field
														key={configs.name}
														validateOnBlur
														validateOnChange
														name={configs.name}
														type={configs.type}
														render={({ field, form }) => (
															<TextField
																name={configs.name}
																type={configs.type}
																label={configs.placeholder}
																error={!!form.errors[configs.name] && !!form.touched[configs.name]}
																onChange={formikBag.handleChange}
																onBlur={formikBag.handleBlur}
																defaultValue={userProfile[configs.name]}
																fullWidth
																margin="normal"
																helperText={
																	form.errors[configs.name] &&
																	form.touched[configs.name] &&
																	form.errors[configs.name]
																}
															/>
														)}
													/>
												))
											}
											<Button type="submit" className="submit-button" variant="outlined" color="primary" disabled={!formikBag.isValid} fullWidth>
												{FORMS.settings.submitButtonText}
											</Button>
										</Form>
									</CardContent>
								</Card>
							)}>
						</Formik> :
						null
					}
				</div>
			</div>
		)
	}
}

const mapStateToProps = (state) => createStructuredSelector({
	userProfile: selectUser()
});
const mapDispatchToProps = dispatch => ({
	updateUserProfile: userProfile => dispatch(updateUserProfile(userProfile))
});

export default connect(mapStateToProps, mapDispatchToProps)(SettingsScreen);