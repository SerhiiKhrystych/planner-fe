import React, { Component } from 'react';
import List from '@material-ui/core/List';
import ListSubheader from '@material-ui/core/ListSubheader';
import Typography from '@material-ui/core/Typography';

import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect';
import { groupBy, uniq } from 'lodash';

import Note from '../../components/note/note';
import CreateNote from '../../components/note/create-note';

import './notes-screen.css'
import '../../App.css';

// Selectors
import { selectNotes, selectFilterByValue } from "../../redux/selectors";

// Actions
import { getNotes, createNote, updateNote, deleteNote, filterNotes } from "../../redux/actions/notes";

// Helpers
import {getReadableDate} from "../../utils/calendar.helper";

import Socket from "../../utils/socket";
import {clearNotifications, updateNotifications} from "../../redux/actions/notifications";

class NotesScreen extends Component {

	componentDidMount() {
		const { getNotes, filterNotes, clearNotifications, updateNotifications } = this.props;
		filterNotes(null);
		getNotes();
		clearNotifications('notes');
		this.socket = new Socket();
		this.socket.on('events', () => {
			updateNotifications('events')
		});
	}

	componentWillUnmount() {
		this.socket.disconnect();
	}

	render() {
		const { notes, filterBy, createNote, updateNote, deleteNote } = this.props;
		const groupedNotes = groupBy(notes, 'createdAt');
		const sections = uniq(notes.map(note => note.createdAt));
		return (
			<div className="app-notes-screen app-main-content-container">
				<List subheader={<li />}>
					{sections.map((section) => (
						<li key={`section-${section}`}>
							<ul className="app-notes-list">
								<ListSubheader>Created {getReadableDate(section)}</ListSubheader>
								{groupedNotes[section].map((note) => (
									<Note key={`item-${sections}-${note.id}`} note={note} onUpdate={updateNote} onDelete={deleteNote} />
								))}
							</ul>
						</li>
					))}
				</List>

				{
					(!notes.length && !!filterBy) ?
					<Typography variant="h6" align="center" color="primary" >
						No results with '{filterBy}'.
					</Typography> : null
				}

				<CreateNote onCreate={createNote} />
			</div>
		)
	}
}

const mapStateToProps = (state) => createStructuredSelector({
	notes: selectNotes(),
	filterBy: selectFilterByValue()
});
const mapDispatchToProps = dispatch => ({
	getNotes:    () => dispatch(getNotes()),
	createNote:  (note) => dispatch(createNote(note)),
	updateNote:  (note) => dispatch(updateNote(note)),
	deleteNote:  (id) => dispatch(deleteNote(id)),
	filterNotes: (filterBy) => dispatch(filterNotes(filterBy)),
	updateNotifications: (type) => dispatch(updateNotifications(type)),
	clearNotifications: (type) => dispatch(clearNotifications(type))
});

export default connect(mapStateToProps, mapDispatchToProps)(NotesScreen);