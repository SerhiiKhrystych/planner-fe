import React, { Component } from 'react';
import { Formik, Form, Field } from 'formik';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';

import CardContent from '@material-ui/core/CardContent';


import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { FORMS } from "../../utils/forms";

import { get } from 'lodash';

import { login, registration } from '../../redux/actions/auth';

import '../../form.css';

class AuthScreen extends Component {

	handleSubmit = (values, authType) => {
		const { login, registration } = this.props;
		switch (authType) {
			case 'forgot-password':
				console.log('This request is not exist yet');
				break;

			case 'login':
				this.login(values);
				break;

			case 'registration':
				this.registration(values);
				break;

			default: {
				console.log('incorrect Auth Type');
			}
		}
	};

	login = (user) => {
		const { login, history } = this.props;
		login(user).then(() => {
			history.push('/');
		}, () => {
			console.log('Login Error');
		})
	};

	registration = (user) => {
		this.props.registration(user).then(() => {
			this.login({ username: user.username, password: user.password });
		}, () => {
			console.log('Registration Error');
		});
	};

	render() {
		const authType = get(this.props, 'match.params.authType', 'login');
		return (
			<div className="form-wrapper">
				{
					<Formik
						initialValues={FORMS[authType].initialValues}
						validationSchema={FORMS[authType].validationSchema}
						onSubmit={(values) => {
							this.handleSubmit(values, authType);
						}}
						render={(formikBag) => (
							<Card className="form">
								<CardContent>
									<Form>
										<Typography variant="h6" align="center" color="primary" >
											{FORMS[authType].formLabel}
										</Typography>
										{
											FORMS[authType].fields.map((configs, key) => (
												<Field
													key={configs.name}
													validateOnBlur
													validateOnChange
													name={configs.name}
													type={configs.type}
													render={({ field, form }) => (
														<TextField
															name={configs.name}
															type={configs.type}
															label={configs.placeholder}
															error={!!form.errors[configs.name] && !!form.touched[configs.name]}
															onChange={formikBag.handleChange}
															onBlur={formikBag.handleBlur}
															fullWidth
															margin="normal"
															helperText={
																form.errors[configs.name] &&
																form.touched[configs.name] &&
																form.errors[configs.name]
															}
														/>
													)}
												/>
											))
										}
										<Button type="submit" className="submit-button" variant="outlined" color="primary" disabled={!formikBag.isValid} fullWidth>
											{FORMS[authType].submitButtonText}
										</Button>
									</Form>
								</CardContent>
							</Card>
						)}>
					</Formik>
				}
			</div>
		)
	}
}

const mapStateToProps = (state) => createStructuredSelector({

});
const mapDispatchToProps = dispatch => ({
	login: user => dispatch(login(user)),
	registration: user => dispatch(registration(user)),
});
export default connect(mapStateToProps, mapDispatchToProps)(AuthScreen);
