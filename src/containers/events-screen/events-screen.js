import React, { Component } from 'react';

import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect';

import Calendar from '../../components/calendar/calendar';

import Grid from '@material-ui/core/Grid';

import './events-screen.css';
import './../../App.css';

import CreateEvent from '../../components/calendar/create-event';

import List from '@material-ui/core/List';
import CardHeader from '@material-ui/core/CardHeader';


import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

import { getReadableDate } from '../../utils/calendar.helper';

import { selectEventsGroupedByDayId } from "../../redux/selectors/events";
import {createEvent, deleteEvent, getEvents, updateEvent} from "../../redux/actions/events";

import Socket from "../../utils/socket";

import Event from '../../components/calendar/event';
import {clearNotifications, updateNotifications} from "../../redux/actions/notifications";

class EventsScreen extends Component {
	socket;
	constructor(props) {
		super(props);

		this.state = {
			selectedDay:     new Date().toDateString()
		};
		this.socket = new Socket();
	}

	componentDidMount() {
		const { getEvents, updateNotifications, clearNotifications } = this.props;
		getEvents();
		clearNotifications('events');

		this.socket.on('notes', () => {
			updateNotifications('notes');
		});
	}

	componentWillUnmount() {
		this.socket.disconnect();
	}

	monthChangeHandler = () => {};

	selectDay = (day) => {
		this.setState({ selectedDay: new Date(day.date).toDateString() });
	};

	createEvent = (event) => {
		const { createEvent } = this.props;
		createEvent(event);
	};

	updateEvent = (event) => {
		const { updateEvent } = this.props;
		updateEvent(event);
	};

	deleteEvent = (id) => {
		const { deleteEvent } = this.props;
		deleteEvent(id);
	};

	render() {
		const { month, monthName, year, selectedDay } = this.state;
		const { events } = this.props;
		return (
			<div className="app-main-content-container app-events-screen">
				<Grid container className="grid-container">
					<Grid item xs={7} sm={7} lg={7} className="app-events-screen--column">
						<Calendar month={month} monthName={monthName} year={year}
						          selectDay={this.selectDay}
						          selectedDay={selectedDay}
						          events={events}
						          onMonthChange={this.monthChangeHandler}>
						</Calendar>
					</Grid>
					<Grid item xs={5} sm={5} lg={5} className="app-events-screen--column">
						<Card className="app-card">
							<CardHeader
								title={ getReadableDate(selectedDay) }
							/>
							<CardContent>
								{
									(events && events[selectedDay]) ?
										<List dense={true}>
											{
												events[selectedDay].map((event, key) =>
													<Event event={event} key={key} onUpdate={this.updateEvent} onDelete={this.deleteEvent} />
												)
											}
										</List>
										: null
								}

							</CardContent>
						</Card>
					</Grid>
					<CreateEvent onCreate={this.createEvent} selectedDay={selectedDay} />
				</Grid>
			</div>
		)
	}
}

const mapStateToProps = (state) => createStructuredSelector({
	events: selectEventsGroupedByDayId()
});
const mapDispatchToProps = dispatch => ({
	getEvents: () => dispatch(getEvents()),
	createEvent: (event) => dispatch(createEvent(event)),
	updateEvent: (event) => dispatch(updateEvent(event)),
	deleteEvent: (id) => dispatch(deleteEvent(id)),
	updateNotifications: (type) => dispatch(updateNotifications(type)),
	clearNotifications: (type) => dispatch(clearNotifications(type))
});

export default connect(mapStateToProps, mapDispatchToProps)(EventsScreen);