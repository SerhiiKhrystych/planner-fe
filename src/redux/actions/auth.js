import { fetchLogin, fetchRegister, fetchUserProfile, fetchUserProfileUpdate } from '../../service/api';

import Auth from '../../utils/auth';

/**
 * login
 * @param user: { username, password }
 */
export const login = (user) => async (dispatch) => {
	dispatch(setLoadedValue('login', false));
	try {
		const response = await fetchLogin(user);
		Auth.setToken(response.data.token);
		dispatch(loginSuccess(response.data.user));
		dispatch(setLoadedValue('login', true));
		return;
	} catch (e) {
		dispatch(loginFail(e));
		dispatch(setLoadedValue('login', true));
		throw new Error(e);
	}
};
export const LOGIN_SUCCESS = '[Auth] Login Success';
const loginSuccess = (user) => ({ type: LOGIN_SUCCESS, payload: { user } });

export const LOGIN_FAIL = '[Auth] Login Fail';
const loginFail = (error) => ({ type: LOGIN_FAIL, payload: { error } });


/**
 * registration
 * @param user: { firstName, lastName, username, email, phoneNumber, password }
 */
export const registration = (user) => async (dispatch) => {
	dispatch(setLoadedValue('registration', false));
	try {
		const response = await fetchRegister(user);
		dispatch(registrationSuccess(response.data.user));
		dispatch(setLoadedValue('registration', true));
		return;
	} catch ({ error }) {
		dispatch(registrationFail(error));
		dispatch(setLoadedValue('registration', true));
		throw new Error(error);
	}
};

export const REGISTRATION_SUCCESS = '[Auth] Registration Success';
const registrationSuccess = (user) => ({ type: REGISTRATION_SUCCESS, payload: { user }});

export const REGISTRATION_FAIL = '[Auth] Registration Fail';
const registrationFail = (error) => ({ type: REGISTRATION_FAIL, payload: { error } });

/**
 * get user profile
 */
export const getUserProfile = () => async (dispatch) => {
	dispatch(setLoadedValue('getProfile', false));
	try {
		const response = await fetchUserProfile();
		dispatch(getUserProfileSuccess(response.data.user));
		dispatch(setLoadedValue('getProfile', true));
	} catch (e) {
		dispatch(getUserProfileFail(e));
		dispatch(setLoadedValue('getProfile', true));
	}
};


/**
 * update user profile
 * @param user
 */
export const updateUserProfile = (user) => async (dispatch) => {
	dispatch(setLoadedValue('updateProfile', false));
	try {
		const response = await fetchUserProfileUpdate(user);
		dispatch(updateUserProfileSuccess(response.data.user));
		dispatch(setLoadedValue('updateProfile', true));
	} catch (e) {
		dispatch(updateUserProfileFail(e));
		dispatch(setLoadedValue('updateProfile', true));
	}
};

export const UPDATE_USER_PROFILE_SUCCESS = '[Auth] Update User Profile Success';
const updateUserProfileSuccess = (user) => ({ type: UPDATE_USER_PROFILE_SUCCESS, payload: { user }});

export const UPDATE_USER_PROFILE_FAIL = '[Auth] Update User Profile Fail';
const updateUserProfileFail = (error) => ({ type: UPDATE_USER_PROFILE_FAIL, payload: { error }});

export const GET_USER_PROFILE_SUCCESS = '[Auth] Get User Profile Success';
const getUserProfileSuccess = (user) => ({ type: GET_USER_PROFILE_SUCCESS, payload: { user }});

export const GET_USER_PROFILE_FAIL = '[Auth] Get User Profile Fail';
const getUserProfileFail = (error) => ({ type: GET_USER_PROFILE_FAIL, payload: { error }});

/**
 * sets 'loaded' value for the action of the specified type
 * type can be 'login', 'registration', 'getProfile', 'updateProfile'
 * @type {string}
 * @loaded {boolean}
 */
export const SET_LOADED = '[Auth] Set Loaded Value';
const setLoadedValue = (loaded) => ({ type: SET_LOADED, payload: { loaded } });