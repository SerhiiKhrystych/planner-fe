import { fetchNotes, fetchNoteNew, fetchNoteUpdate, fetchNoteDelete } from '../../service/api';

/**
 * get all notes
 */
export const getNotes = () => async (dispatch) => {
	dispatch(setLoadedValue('getNotes', false));
	try {
		const response = await fetchNotes();
		dispatch(getNotesSuccess(response.data.notes));
		dispatch(setLoadedValue('getNotes', true));
	} catch (e) {
		dispatch(getNotesFail(e));
		dispatch(setLoadedValue('getNotes', true));
	}
};

export const GET_NOTES_SUCCESS = '[Notes] Get Notes Success';
const getNotesSuccess = (list) => ({ type: GET_NOTES_SUCCESS, payload: { list } });

export const GET_NOTES_FAIL = '[Notes] Get Notes Fail';
const getNotesFail = (error) => ({ type: GET_NOTES_FAIL, payload: { error } });

/**
 * create new note
 * @param note: { title, priority, description }
 */
export const createNote = (note) => async (dispatch) => {
	dispatch(setLoadedValue('createNote', false));
	try {
		const response = await fetchNoteNew(note);
		dispatch(createNoteSuccess(response.data.note));
		dispatch(setLoadedValue('createNote', true));
	} catch ({ error }) {
		dispatch(createNoteFail(error));
		dispatch(setLoadedValue('createNote', true));
	}
};

export const CREATE_NOTE_SUCCESS = '[Notes] Create Note Success';
const createNoteSuccess = (note) => ({ type: CREATE_NOTE_SUCCESS, payload: { note } });

export const CREATE_NOTE_FAIL = '[Notes] Create Note Fail';
const createNoteFail = (error) => ({ type: CREATE_NOTE_FAIL, payload: { error } });

/**
 * update note
 * @param note: { title, priority, description }
 */
export const updateNote = (note) => async (dispatch) => {
	dispatch(setLoadedValue('updateNote', false));
	try {
		const response = await fetchNoteUpdate(note.id, note);
		dispatch(updateNoteSuccess(response.data.note));
		dispatch(setLoadedValue('updateNote', true));
	} catch (e) {
		dispatch(updateNoteFail(e));
		dispatch(setLoadedValue('updateNote', true));
	}
};

export const UPDATE_NOTE_SUCCESS = '[Notes] Update Note Success';
const updateNoteSuccess = (note) => ({ type: UPDATE_NOTE_SUCCESS, payload: { note }});

export const UPDATE_NOTE_FAIL = '[Notes] Update Note Fail';
const updateNoteFail = (error) => ({ type: UPDATE_NOTE_FAIL, payload: { error }});

/**
 * delete note
 * @param id
 */
export const deleteNote = (id) => async (dispatch) => {
	dispatch(setLoadedValue('deleteNote', false));

	try {
		const response = await fetchNoteDelete(id);
		dispatch(deleteNoteSuccess(id));
		dispatch(setLoadedValue('deleteNote', true));
	} catch (e) {
		dispatch(deleteNoteFail(e));
		dispatch(setLoadedValue('deleteNote', true));
	}
};

export const DELETE_NOTE_SUCCESS = '[Notes] Delete Note Success';
const deleteNoteSuccess = (id) => ({ type: DELETE_NOTE_SUCCESS, payload: { id }});

export const DELETE_NOTE_FAIL = '[Notes] Delete Note Fail';
const deleteNoteFail = (error) => ({ type: DELETE_NOTE_FAIL, payload: { error }});

export const FILTER_NOTES = '[Notes] Filter Notes';
export const filterNotes = (filterBy) => ({ type: FILTER_NOTES, payload: { filterBy }});

/**
 * sets 'loaded' value for the action of the specified type
 * type can be 'getNotes', 'createNote', 'updateNote', 'deleteNote'
 * @type {string}
 * @loaded {boolean}
 */
export const SET_LOADED = '[Notes] Set Loaded Value';
const setLoadedValue = (actionType, loaded) => ({ type: SET_LOADED, payload: { actionType, loaded } });