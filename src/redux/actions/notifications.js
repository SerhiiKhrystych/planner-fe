export const UPDATE_NOTIFICATIONS = '[Notifications] Update';
export const updateNotifications = (type) => ({ type: UPDATE_NOTIFICATIONS, payload: { type } });

export const CLEAR_NOTIFICATIONS = '[Notifications] Clear';
export const clearNotifications = (type) => ({ type: CLEAR_NOTIFICATIONS, payload: { type } });