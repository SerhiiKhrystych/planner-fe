import { fetchEvents, fetchEventNew, fetchEventUpdate, fetchEventDelete } from '../../service/api';
import {generateDayId} from "../../utils/calendar.helper";

/**
 * get all event
 */
export const getEvents = () => async (dispatch) => {
	dispatch(setLoadedValue('getList', false));
	try {
		const response = await fetchEvents();
		const events = response.data.events.map(event => ({
			...event,
			stringData: new Date(event.date).toDateString()
		}));
		dispatch(getEventsSuccess(events));
		dispatch(setLoadedValue('getList', true));
	} catch (e) {
		dispatch(getEventsFail(e));
		dispatch(setLoadedValue('getList', true));
	}
};

export const GET_EVENTS_SUCCESS = '[Events] Get Events Success';
const getEventsSuccess = (list) => ({ type: GET_EVENTS_SUCCESS, payload: { list } });

export const GET_EVENTS_FAIL = '[Events] Get Events Fail';
const getEventsFail = (error) => ({ type: GET_EVENTS_FAIL, payload: { error } });

/**
 * create new event
 * @param event
 */
export const createEvent = (event) => async (dispatch) => {
	dispatch(setLoadedValue('createEvent', false));
	try {
		const response = await fetchEventNew(event);
		dispatch(createEventSuccess({
			...response.data.event,
			stringData: new Date(response.data.event.date).toDateString()
		}));
		dispatch(setLoadedValue('createEvent', true));
	} catch (e) {
		dispatch(createEventFail(e));
		dispatch(setLoadedValue('createEvent', true));
	}
};

export const CREATE_EVENT_SUCCESS = '[Events] Create Event Success';
const createEventSuccess = (event) => ({ type: CREATE_EVENT_SUCCESS, payload: { event }});

export const CREATE_EVENT_FAIL = '[Events] Create Event Fail';
const createEventFail = (error) => ({ type: CREATE_EVENT_FAIL, payload: { error }});


/**
 * update event
 * @param event
 */
export const updateEvent = (event) => async (dispatch) => {
	dispatch(setLoadedValue('updateEvent', false));
	try {
		const response = await fetchEventUpdate(event);
		dispatch(updateEventSuccess({
			...response.data.event,
			stringData: new Date(response.data.event.date).toDateString()
		}));
		dispatch(setLoadedValue('updateEvent', true));
		return;
	} catch (e) {
		dispatch(updateEventFail(e));
		dispatch(setLoadedValue('updateEvent', true));
		throw new Error(e);
	}
};

export const UPDATE_EVENT_SUCCESS = '[Events] Update Event Success';
const updateEventSuccess = (event) => ({ type: UPDATE_EVENT_SUCCESS, payload: { event } });

export const UPDATE_EVENT_FAIL = '[Events] Update Event Fail';
const updateEventFail = (error) => ({ type: UPDATE_EVENT_FAIL, payload: { error } });

/**
 * delete event
 * @param id
 */
export const deleteEvent = (id) => async (dispatch) => {
	dispatch(setLoadedValue('deleteEvent', false));
	try {
		const response = await fetchEventDelete(id);
		dispatch(deleteEventSuccess(id));
		dispatch(setLoadedValue('deleteEvent', true));
	} catch (e) {
		dispatch(deleteEventFail(e));
		dispatch(setLoadedValue('deleteEvent', true));
	}
};

export const DELETE_EVENT_SUCCESS = '[Events] Delete Event Success';
const deleteEventSuccess = (id) => ({ type: DELETE_EVENT_SUCCESS, payload: { id }});

export const DELETE_EVENT_FAIL = '[Events] Delete Event Fail';
const deleteEventFail = (error) => ({ type: DELETE_EVENT_FAIL, payload: { error }});


/**
 * sets 'loaded' value for the action of the specified type
 * type can be 'getList', 'createEvent', 'updateEvent', 'deleteEvent'
 * @type {string}
 * @loaded {boolean}
 */
export const SET_LOADED = '[Events] Set Loaded Value';
const setLoadedValue = (actionType, loaded) => ({ type: SET_LOADED, payload: { actionType, loaded } });
