import {
	GET_EVENTS_SUCCESS,
	GET_EVENTS_FAIL,
	CREATE_EVENT_SUCCESS,
	CREATE_EVENT_FAIL,
	UPDATE_EVENT_SUCCESS,
	UPDATE_EVENT_FAIL,
	DELETE_EVENT_SUCCESS,
	DELETE_EVENT_FAIL,
	SET_LOADED
} from '../actions/events';

const initialState = {
	list:   [],
	loaded: {
		getList:     true,
		createEvent: true,
		updateEvent: true,
		deleteEvent: true
	},
	error:  null
};
export default function events(state = initialState, action) {
	switch (action.type) {
		case GET_EVENTS_SUCCESS: {
			const list = [ ...action.payload.list ];
			return {
				...state,
				list
			};
		}

		case CREATE_EVENT_SUCCESS: {
			const list = [
				...state.list,
				action.payload.event
			];
			return {
				...state,
				list
			};
		}

		case UPDATE_EVENT_SUCCESS: {
			const list = [ ...state.list ].map(event => event.id === action.payload.event.id ? action.payload.event : event);
			return {
				...state,
				list
			};
		}

		case DELETE_EVENT_SUCCESS: {
			const list = [ ...state.list ].filter(event => event.id !== action.payload.id);
			return {
				...state,
				list
			};
		}

		case GET_EVENTS_FAIL:
		case CREATE_EVENT_FAIL:
		case UPDATE_EVENT_FAIL:
		case DELETE_EVENT_FAIL: {
			return {
				...state,
				error: action.payload.error
			};
		}

		case SET_LOADED: {
			const loaded = {
				...state.loaded,
				[action.payload.actionType]: action.payload.loaded
			};
			return {
				...state,
				loaded
			};
		}

		default: {
			return {
				...state
			}
		}
	}
}