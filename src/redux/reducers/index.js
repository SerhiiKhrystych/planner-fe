import { combineReducers } from 'redux';

import notes from './notes';
import events from './events';
import auth from './auth';
import notifications from './notifications';

export default combineReducers({
	notes,
	events,
	auth,
	notifications
});