import {
	LOGIN_SUCCESS,
	LOGIN_FAIL,
	REGISTRATION_SUCCESS,
	REGISTRATION_FAIL,
	GET_USER_PROFILE_SUCCESS,
	GET_USER_PROFILE_FAIL,
	UPDATE_USER_PROFILE_SUCCESS,
	UPDATE_USER_PROFILE_FAIL,
	SET_LOADED
} from '../actions/auth';

const initialState = {
	user:   null,
	loaded: {
		login:          true,
		registration:   true,
		getProfile:     true,
		updateProfile:  true
	},
	error:  null,
};
export default function auth(state = initialState, action) {
	switch (action.type) {
		case LOGIN_SUCCESS:
		case REGISTRATION_SUCCESS:
		case GET_USER_PROFILE_SUCCESS:
		case UPDATE_USER_PROFILE_SUCCESS:
			return {
				...state,
				user: action.payload.user
			};

		case LOGIN_FAIL:
		case REGISTRATION_FAIL:
		case GET_USER_PROFILE_FAIL:
		case UPDATE_USER_PROFILE_FAIL:
			return {
				...state,
				error: action.payload.error
			};

		case SET_LOADED: {
			const loaded = {
				...state.loaded,
				[action.payload.actionType]: action.payload.loaded
			};
			return {
				...state,
				loaded
			};
		}

		default: {
			return {
				...state
			}
		}
	}
}