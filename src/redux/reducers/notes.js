import {
	GET_NOTES_SUCCESS,
	GET_NOTES_FAIL,
	CREATE_NOTE_SUCCESS,
	CREATE_NOTE_FAIL,
	UPDATE_NOTE_SUCCESS,
	UPDATE_NOTE_FAIL,
	DELETE_NOTE_SUCCESS,
	DELETE_NOTE_FAIL,
	FILTER_NOTES,
	SET_LOADED
} from '../actions/notes';

const initialState = {
	list:       [],
	loaded:     true,
	filterBy:   '',
	error:  {
		getNotes:   true,
		createNote: true,
		updateNote: true,
		deleteNote: true
	}
};
export default function notes(state = initialState, action) {
	switch (action.type) {
		case GET_NOTES_SUCCESS: {
			const list = [ ...action.payload.list ];
			return {
				...state,
				list
			};
		}

		case CREATE_NOTE_SUCCESS: {
			const list = [ ...state.list, action.payload.note ];
			return {
				...state,
				list
			}
		}

		case UPDATE_NOTE_SUCCESS: {
			const list = [ ...state.list ].map(note => action.payload.note.id === note.id ? action.payload.note : note);
			return {
				...state,
				list
			}
		}

		case DELETE_NOTE_SUCCESS: {
			const list = [ ...state.list ].filter(note => note.id !== action.payload.id);
			return {
				...state,
				list
			}
		}

		case FILTER_NOTES: {
			return {
				...state,
				filterBy: action.payload.filterBy
			}
		}

		case GET_NOTES_FAIL:
		case CREATE_NOTE_FAIL:
		case UPDATE_NOTE_FAIL:
		case DELETE_NOTE_FAIL:
			return {
				...state,
				error: action.payload.error
			};

		case SET_LOADED: {
			const loaded = {
				...state.loaded,
				[action.payload.actionType]: action.payload.loaded
			};
			return {
				...state,
				loaded
			};
		}

		default: {
			return {
				...state
			}
		}
	}
}