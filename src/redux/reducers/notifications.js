import {
	UPDATE_NOTIFICATIONS,
	CLEAR_NOTIFICATIONS
} from "../actions/notifications"

const initialState = {
	events:     0,
	notes:      0,
};
export default function notifications(state = initialState, action) {
	switch (action.type) {
		case UPDATE_NOTIFICATIONS: {
			return {
				...state,
				[action.payload.type]: state[action.payload.type] + 1
			};
		}

		case CLEAR_NOTIFICATIONS: {
			console.log('CLEAR_NOTIFICATIONS', CLEAR_NOTIFICATIONS);
			return {
				...state,
				[action.payload.type]: 0
			}
		}

		default: {
			return {
				...state
			}
		}
	}
}