import { createSelector } from 'reselect'

const selectNotificationsDomain = (state) => state.notifications;

export const selectNotesNotifications = () => createSelector(selectNotificationsDomain, subdomain => subdomain.notes);
export const selectEventsNotifications = () => createSelector(selectNotificationsDomain, subdomain => subdomain.events);
