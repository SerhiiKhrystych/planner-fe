import { createSelector } from 'reselect';

const selectAuthDomain = (state) => state.auth;

export const selectUser = () => createSelector(selectAuthDomain, subdomain => subdomain.user);