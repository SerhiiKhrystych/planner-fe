import { createSelector } from 'reselect'

const selectNotesDomain = (state) => state.notes;

export const selectNotes = () => createSelector(selectNotesDomain, subdomain => {
	if (!subdomain.filterBy || subdomain.filterBy.length < 3) return subdomain.list;

	return subdomain.list.filter(item => item.title.toLowerCase().indexOf(subdomain.filterBy.toLowerCase()) >= 0 || item.description.toLowerCase().indexOf(subdomain.filterBy.toLowerCase()) >= 0)
});

export const selectFilterByValue = () => createSelector(selectNotesDomain, subdomain => subdomain.filterBy);