import { createSelector } from 'reselect';
import { groupBy } from 'lodash';


const selectEventsDomain = (state) => state.events;

export const selectEventsGroupedByDayId = () => createSelector(selectEventsDomain, subdomain => groupBy(subdomain.list, 'stringData'));