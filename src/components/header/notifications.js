import React, { Component } from 'react';

import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

class Notifications extends Component {

	render() {
		const { anchorEl, open, handleClose, navigate, notes, events } = this.props;
		return (
			<Menu
				anchorEl={anchorEl}
				anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
				transformOrigin={{ vertical: 'top', horizontal: 'right' }}
				open={open}
				onClose={handleClose}>
				<MenuItem onClick={() => { navigate('notes'); handleClose(); }}>Notes: {notes}</MenuItem>
				<MenuItem onClick={() => { navigate('events'); handleClose();}}>Events: {events}</MenuItem>
			</Menu>
		)
	}
}

export default Notifications;