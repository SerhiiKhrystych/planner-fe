import React, { Component } from 'react';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import NotesIcon from '@material-ui/icons/Notes';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';

class SideList extends Component {

	render() {
		const { onOpen, onClose, open, classes, history, menuNavigate } = this.props;

		return (
			<SwipeableDrawer
				onClose={onClose}
				onOpen={onOpen}
				open={open}>
				<div className={classes.list}>
					<List>
						<ListItem disabled={history.location.pathname === '/events'} selected={history.location.pathname === '/events'} button onClick={menuNavigate(history, 'events')}>
							<ListItemIcon>
								<CalendarTodayIcon />
							</ListItemIcon>
							<ListItemText primary="Events" />
						</ListItem>
						<ListItem disabled={history.location.pathname === '/notes'} selected={history.location.pathname === '/notes'} button onClick={menuNavigate(history, 'notes')}>
							<ListItemIcon>
								<NotesIcon />
							</ListItemIcon>
							<ListItemText primary="Notes" />
						</ListItem>
					</List>
				</div>
			</SwipeableDrawer>
		)
	}
}

export default SideList;