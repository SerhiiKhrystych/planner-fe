import React, { Component } from 'react';

import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

class FullsizeMenu extends Component {
	render() {
		const { anchorEl, open, profileNavigate, logOut, handleMenuClose, currentRoute } = this.props;
		return (
			<Menu
				anchorEl={anchorEl}
				anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
				transformOrigin={{ vertical: 'top', horizontal: 'right' }}
				open={open}
				onClose={handleMenuClose}
			>
				<MenuItem onClick={() => { profileNavigate(); handleMenuClose();}} disabled={currentRoute === '/settings'} selected={currentRoute === '/settings'}>Settings</MenuItem>
				<MenuItem onClick={() => { logOut(); handleMenuClose(); }}>Log Out</MenuItem>
			</Menu>
		)
	}
}

export default FullsizeMenu;
