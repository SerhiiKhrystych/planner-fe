import React, { Component } from 'react';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import InputBase from '@material-ui/core/InputBase';
import Badge from '@material-ui/core/Badge';

import { withStyles } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import AccountCircle from '@material-ui/icons/AccountCircle';
import NotificationsIcon from '@material-ui/icons/Notifications';
import MoreIcon from '@material-ui/icons/MoreVert';

import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { filterNotes } from '../../redux/actions/notes';
import { selectUser } from "../../redux/selectors/auth";
import { selectEventsNotifications, selectNotesNotifications } from "../../redux/selectors/notifications";


import SideBar from './side-bar';
import MobileMenu from './mobile-menu';
import FullsizeMenu from './fullsize-menu';
import Notifications from './notifications';

import Auth from '../../utils/auth';


import styles from './styles';

class Header extends Component {
	// socket;
	state = {
		anchorEl: null,
		mobileMoreAnchorEl: null,
		notificationsAnchorEl: null,
		navbarOpened: false,
	};

	handleProfileMenuOpen = event => {
		this.setState({ anchorEl: event.currentTarget });
	};

	handleNotificationsOpen = event => {
		this.setState({ notificationsAnchorEl: event.currentTarget });
	};

	handleMenuClose = () => {
		this.setState({ anchorEl: null });
	};

	handleMobileMenuOpen = event => {
		this.setState({ mobileMoreAnchorEl: event.currentTarget });
	};

	handleMobileMenuClose = () => {
		this.setState({ mobileMoreAnchorEl: null });
	};

	handleNotificationsClose = () => {
		this.setState({ notificationsAnchorEl: null });
	};

	sidebarOpen = () => {
		this.setState({ navbarOpened: true });
	};

	sidebarClose = () => {
		this.setState({ navbarOpened: false });
	};

	menuNavigate = (history, path) => () => {
		history.push(path);
		this.sidebarClose();
	};

	filterNotesChangeHandler = (event) => {
		this.props.filterNotes(event.target.value);
	};

	logOut = (history) => () => {
		Auth.removeToken();
		history.push('auth/login');
		this.sidebarClose();
	};

	navigateByNotifications = (page) => {
		const { history } = this.props;

		history.push(page);
		this.setState({ notifications: { ...this.state.notifications, [page]: 0 }});
	};

	render() {
		const { anchorEl, mobileMoreAnchorEl, notificationsAnchorEl } = this.state;
		const { history, classes, notesNotifications, eventsNotifications } = this.props;

		return (
			(history.location.pathname === '/events' || history.location.pathname === '/notes' || history.location.pathname === '/settings') ?
				<div className={classes.root}>
					<AppBar position="static">
						<Toolbar>
							<IconButton className={classes.menuButton} color="inherit" aria-label="Open drawer" onClick={this.sidebarOpen}>
								<MenuIcon />
							</IconButton>
							{
								history.location.pathname === '/notes' &&
								<div className={classes.search}>
									<div className={classes.searchIcon}>
										<SearchIcon />
									</div>
									<InputBase
										placeholder="Search…"
										onChange={this.filterNotesChangeHandler}
										classes={{
											root: classes.inputRoot,
											input: classes.inputInput,
										}}
									/>
								</div>
							}
							<div className={classes.grow} />
							<div className={classes.sectionDesktop}>
								<IconButton
									aria-owns={!!notificationsAnchorEl ? 'material-appbar' : undefined}
									aria-haspopup="true"
									onClick={this.handleNotificationsOpen}
									disabled={!(notesNotifications + eventsNotifications)}
						            color="inherit">
									<Badge badgeContent={notesNotifications + eventsNotifications} color="secondary">
										<NotificationsIcon />
									</Badge>
								</IconButton>
								<IconButton
									aria-owns={!!anchorEl ? 'material-appbar' : undefined}
									aria-haspopup="true"
									onClick={this.handleProfileMenuOpen}
									color="inherit">
									<AccountCircle />
								</IconButton>
							</div>
							<div className={classes.sectionMobile}>
								<IconButton aria-haspopup="true" onClick={this.handleMobileMenuOpen} color="inherit">
									<MoreIcon />
								</IconButton>
							</div>
						</Toolbar>
					</AppBar>

					<FullsizeMenu
						anchorEl={anchorEl}
						open={!!anchorEl}
						handleMenuClose={this.handleMenuClose}
						profileNavigate={this.menuNavigate(history, 'settings')}
						logOut={this.logOut(history)}
						currentRoute={history.location.pathname}
					/>

					<MobileMenu anchorEl={mobileMoreAnchorEl} open={!!mobileMoreAnchorEl} handleMenuClose={this.handleMobileMenuClose} handleProfileMenuOpen={this.handleProfileMenuOpen} handleNotificationsOpen={this.handleNotificationsOpen} />

					<Notifications anchorEl={notificationsAnchorEl} open={!!notificationsAnchorEl} handleClose={this.handleNotificationsClose} navigate={this.navigateByNotifications} notes={notesNotifications} events={eventsNotifications} />

					<SideBar onOpen={this.sidebarOpen} onClose={this.sidebarClose} open={this.state.navbarOpened} classes={classes} history={history} menuNavigate={this.menuNavigate} />
				</div> :
				null
		)
	}
}

const mapStateToProps = (state) => createStructuredSelector({
	userProfile: selectUser(),
	notesNotifications: selectNotesNotifications(),
	eventsNotifications: selectEventsNotifications()
});
const mapDispatchToProps = dispatch => ({
	filterNotes: (filterBy) => dispatch(filterNotes(filterBy))
});
export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(Header));