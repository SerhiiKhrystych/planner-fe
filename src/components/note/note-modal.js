import React, { Component } from 'react';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';

import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';

import { FORMS } from "../../utils/forms";
import '../../form.css';
import './note.css'

class NoteModal extends Component {

	render() {
		const { open, note, onClose, onSubmit } = this.props;
		return (
			<Dialog
				open={open}
				onClose={onClose}
				aria-labelledby="form-dialog-title">
				<Formik
					initialValues={{ title: !!note ? note.title : '', description: !!note ? note.description : '', priority: !!note ? note.priority : 'Middle' }}
					validationSchema={FORMS.note.validationSchema}
					onSubmit={({ title, description, priority }) => {
						onSubmit({ title, description, priority });
						onClose();
					}}
					render={(formikBag) => (
						<div>
							<DialogTitle id="form-dialog-title">{FORMS.note.formLabel}</DialogTitle>
							<Form className="form">
								<DialogContent className="app-note-dialog-content">

									{
										FORMS.note.fields.map((configs, key) => (
											configs.type === 'text' ?
												<Field
													key={configs.name}
													validateOnBlur
													validateOnChange
													name={configs.name}
													type={configs.type}
													render={({ field, form }) => (
														<TextField
															name={configs.name}
															type={configs.name}
															rows={configs.rows}
															label={configs.placeholder}
															onChange={formikBag.handleChange}
															onBlur={formikBag.handleBlur}
															defaultValue={!!note ? note[configs.name] : ''}
															fullWidth
															margin="normal"
														/>
													)}
												/> :
												configs.type === 'radio' ?
													<Field
														key={configs.name}
														validateOnBlur
														validateOnChange
														name={configs.name}
														type="text"
														render={({ field, form }) => (
															<FormControl component="fieldset"  margin="normal">
																<FormLabel component="legend">{configs.label}</FormLabel>
																<RadioGroup
																	aria-label={configs.name}
																	name={configs.name}
																	defaultValue={!!note ? note[configs.name] : 'Middle'}
																	onChange={formikBag.handleChange}>
																	{
																		configs.options.map((option, i) => (
																			<FormControlLabel
																				key={i}
																				value={option}
																				control={<Radio color="primary" />}
																				label={option}
																				labelPlacement="end"
																			/>
																		))
																	}
																</RadioGroup>
															</FormControl>
														)}
													/> : null
										))
									}

								</DialogContent>
								<DialogActions>
									<Button onClick={onClose} color="primary">
										{FORMS.note.cancelButtonText}
									</Button>
									<Button type="submit" color="primary" disabled={!formikBag.isValid}>
										{FORMS.note.submitButtonText}
									</Button>
								</DialogActions>
							</Form>
						</div>
					)}>
				</Formik>


			</Dialog>
		)
	}
}

export default NoteModal;