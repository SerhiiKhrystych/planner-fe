import React, { Component } from 'react';

import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';

import NoteModal from './note-modal';

class CreateNote extends Component {
	state = {
		opened: false
	};

	toggleCreateModal = (opened) => {
		this.setState({ opened });
	};

	render() {
		const { onCreate } = this.props;
		const { opened } = this.state;

		return (
			<div>
				<Fab color="primary" aria-label="Add Note" className="app-add-note-button" onClick={() => { this.toggleCreateModal(true) }}>
					<AddIcon />
				</Fab>
				<NoteModal open={opened} onClose={() => { this.toggleCreateModal(false) }} onSubmit={onCreate} />
			</div>

		)
	}
}

export default CreateNote;