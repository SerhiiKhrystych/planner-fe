import React, { Component } from 'react';

import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import PriorityHighIcon from '@material-ui/icons/PriorityHigh';
import LowPriorityIcon from '@material-ui/icons/LowPriority';
import Avatar from '@material-ui/core/Avatar';

import '../../form.css';
import './note.css'

import NoteModal from './note-modal';
import ConfirmModal from '../confirm-modal/confirm-modal';

class Note extends Component {
	state = {
		open: {
			read: false,
			update: false,
			delete: false
		}
	};

	handleClose = (modalType) => {
		this.setState({ open: { ...this.state.open, [modalType]: false } });
	};

	handleOpen = (modalType) => () => {
		this.setState({ open: { ...this.state.open, [modalType]: true } });
	};

	render() {
		const { note, onUpdate, onDelete } = this.props;
		return (
			<div>
				<ListItem>
					<ListItemAvatar>
						<Avatar>
							{
								note.priority === 'Low' && <LowPriorityIcon />
							}
							{
								note.priority === 'High' && <PriorityHighIcon />
							}
						</Avatar>
					</ListItemAvatar>
					<ListItemText primary={`${note.title}`} secondary={note.description} />
					<ListItemSecondaryAction>
						<IconButton aria-label="Edit" onClick={this.handleOpen('update')}>
							<EditIcon />
						</IconButton>
						<IconButton aria-label="Delete" onClick={this.handleOpen('delete')}>
							<DeleteIcon />
						</IconButton>
					</ListItemSecondaryAction>
				</ListItem>
				<ConfirmModal
					title="Delete note?"
					question={ `You want to delete '${note.title}' note?` }
					open={this.state.open.delete}
					onClose={() => { this.handleClose('delete') }}
					confirm={() => { onDelete(note.id) }}
				/>
				<NoteModal note={note} open={this.state.open.update} onClose={() => { this.handleClose('update') }} onSubmit={(updatedNote) => { onUpdate({ ...updatedNote, id: note.id }); this.handleClose('update') }} />
			</div>

		)
	}
}
export default Note;