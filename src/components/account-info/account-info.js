import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';

import './account-info.css'
import { selectUser } from "../../redux/selectors/auth";
import Auth from '../../utils/auth';

class AccountInfo extends Component {
	state = {
		visibleHiddenPart: false
	};

	toggleHiddenPart = () => {
		this.setState({visibleHiddenPart: !this.state.visibleHiddenPart})
	};

	logOut = (history) => {
		Auth.removeToken();
		// history.replace('login');
	};

	navigateOnSettingsPage = (history) => {
		if (history.location.pathname !== '/settings') {
			history.push('settings');
			this.toggleHiddenPart();
		}
	};

	render() {
		const { visibleHiddenPart } = this.state;
		const { history, userProfile } = this.props;
		return (
			<div className="account-info">
				<div className="visible-part" onClick={this.toggleHiddenPart}>
					{ !!userProfile ? `${userProfile.firstName} ${userProfile.lastName}` : '' }
				</div>
				<div className={`hidden-part${visibleHiddenPart ? ' visible' : ''}`}>
					{/*className={`link${history.location.pathname === '/settings' ? ' active' : ''}`} onClick={() => this.navigateOnSettingsPage(history)}*/}
					<NavLink to="/settings" className="link" activeClassName="active">Settings</NavLink>
					<div className="link" onClick={() => this.logOut(history)}>Log Out</div>
				</div>
			</div>
		)
	}
}
const mapStateToProps = (state) => createStructuredSelector({
	userProfile: selectUser()
});
const mapDispatchToProps = dispatch => ({});
export default connect(mapStateToProps, mapDispatchToProps)(AccountInfo);