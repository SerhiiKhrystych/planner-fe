import React, { Component } from 'react';

import ListItem from '@material-ui/core/ListItem';
import Typography from '@material-ui/core/Typography';
import ListItemText from '@material-ui/core/ListItemText';

import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';

import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';

import ConfirmModal from '../confirm-modal/confirm-modal';
import EventModal from './event-modal';

import {getCurrentDate} from "../../utils/calendar.helper";

class Event extends Component {


	state = {
		open: {
			read: false,
			update: false,
			delete: false
		}
	};

	handleClose = (modalType) => {
		this.setState({ open: { ...this.state.open, [modalType]: false } });
	};

	handleOpen = (modalType) => () => {
		this.setState({ open: { ...this.state.open, [modalType]: true } });
	};

	render() {
		const { event = {}, onUpdate, onDelete } = this.props;
		const isEditDisabled = (event && event.date) ? new Date(event.date).valueOf() < getCurrentDate() : true;
		return (
			<div>
				<ListItem>
					<ListItemText
						primary={event.title}
						secondary={event.description}
					/>
					<ListItemSecondaryAction>
						<IconButton aria-label="Edit" onClick={this.handleOpen('update')} disabled={isEditDisabled}>
							<EditIcon />
						</IconButton>
						<IconButton aria-label="Delete" onClick={this.handleOpen('delete')}>
							<DeleteIcon />
						</IconButton>
					</ListItemSecondaryAction>
				</ListItem>

				<ConfirmModal
					title="Delete event?"
					question={ `You want to delete '${event.title}' event?` }
					open={this.state.open.delete}
					onClose={() => { this.handleClose('delete') }}
					confirm={() => { this.handleClose('delete'); onDelete(event.id); }}
				/>

				<EventModal open={this.state.open.update} event={event} onClose={() => { this.handleClose('update') }} onSubmit={(updatedEvent) => { this.handleClose('update'); onUpdate({ ...updatedEvent, id: event.id }); }} />
			</div>

		)
	}
}

export default Event;