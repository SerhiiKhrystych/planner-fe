import React, { Component } from 'react';
import { Formik, Form, Field } from 'formik';

import 'date-fns';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider, DatePicker } from 'material-ui-pickers';

import { FORMS } from "../../utils/forms";
import '../../form.css';
import './calendar.css'

class EventModal extends Component {
	render() {
		const { open, event, onClose, onSubmit } = this.props;
		return (
			<Dialog
				open={open}
				onClose={onClose}
				aria-labelledby="form-dialog-title">
				<Formik
					initialValues={{ title: !!event ? event.title : '', description: !!event ? event.description : '', date: !!event ? event.date : new Date().toDateString() }}
					validationSchema={FORMS.event.validationSchema}
					onSubmit={({ title, description, date }) => {
						onSubmit({ title, description, date });
						onClose();
					}}
					render={(formikBag) => (
						<div>
							<DialogTitle id="form-dialog-title">{FORMS.event.formLabel}</DialogTitle>
							<Form className="form">
								<DialogContent className="app-event-dialog-content">

									{
										FORMS.event.fields.map((configs, key) => (
											configs.type === 'text' ?
												<Field
													key={configs.name}
													validateOnBlur
													validateOnChange
													name={configs.name}
													type={configs.type}
													render={({ field, form }) => (
														<TextField
															name={configs.name}
															type={configs.name}
															rows={configs.rows}
															label={configs.placeholder}
															onChange={formikBag.handleChange}
															onBlur={formikBag.handleBlur}
															defaultValue={!!event ? event[configs.name] : ''}
															fullWidth
															margin="normal"
														/>
													)}
												/> :
												configs.type === 'date' ?
													<Field
														key={configs.name}
														validateOnBlur
														validateOnChange
														name={configs.name}
														type="text"
														render={({ field, form }) => (
															<MuiPickersUtilsProvider utils={DateFnsUtils}>

																<DatePicker
																	label={configs.placeholder}
																	fullWidth
																	margin="normal"
																	value={formikBag.values[configs.name]}
																	disablePast
																	onChange={(event) => {
																		formikBag.setFieldValue(configs.name, event)
																	}}
																	onBlur={formikBag.handleBlur}
																/>
															</MuiPickersUtilsProvider>

														)}
													/> : null
										))
									}

								</DialogContent>
								<DialogActions>
									<Button onClick={onClose} color="primary">
										{FORMS.note.cancelButtonText}
									</Button>
									<Button type="submit" color="primary" disabled={!formikBag.isValid}>
										{FORMS.note.submitButtonText}
									</Button>
								</DialogActions>
							</Form>
						</div>
					)}>
				</Formik>
			</Dialog>
		)
	}
}

export default EventModal;