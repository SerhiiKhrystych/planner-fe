import React,  { Component } from 'react';

import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';

import EventModal from './event-modal';

import './calendar.css';

class CreateEvent extends Component {

	state = {
		opened: false
	};

	toggleCreateModal = (opened) => {
		this.setState({ opened });
	};

	render() {
		const { opened } = this.state;
		const { onCreate, selectedDay } = this.props;
		return (
			<div className="app-add-event-button">
				<Fab color="primary" aria-label="Add Note" onClick={() => { this.toggleCreateModal(true) }}>
					<AddIcon />
				</Fab>

				<EventModal open={opened} event={{date: new Date(selectedDay) }} onClose={() => { this.toggleCreateModal(false) }} onSubmit={(event) => { this.toggleCreateModal(false); onCreate(event); }} />
			</div>
		)
	}
}

export default CreateEvent;