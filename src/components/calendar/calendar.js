/* eslint-disable import/first */
import React, { Component } from 'react';

import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardHeader from '@material-ui/core/CardHeader';

import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

import IconButton from '@material-ui/core/IconButton';
import ArrowRight from '@material-ui/icons/ArrowRight';
import ArrowLeft from '@material-ui/icons/ArrowLeft';

import Badge from '@material-ui/core/Badge';
import Button from '@material-ui/core/Button';
import MailIcon from '@material-ui/icons/Mail';

import Fab from '@material-ui/core/Fab';



import { WeekDays, getCurrentYear, getCurrentMonth, getMonthName, getMonthDays, getCurrentDate, generateDayId } from '../../utils/calendar.helper';

import './calendar.css';
// import io from "../../service/io";

class Calendar extends Component {
	currentDay = new Date().toDateString();

	constructor(props) {
		super(props);
		this.state = {
			month: getCurrentMonth(),
			monthName: getMonthName(),
			year: getCurrentYear(),
			monthDays: getMonthDays(),
		}
	}

	changeMonth(step) {
		let nextMonth = this.state.month + step;
		let nextYear = this.state.year;

		if (nextMonth > 11) {
			nextMonth = 0;
			nextYear += 1;
		} else if (nextMonth < 0) {
			nextMonth = 11;
			nextYear -= 1;
		}

		this.setState({month: nextMonth, year: nextYear}, () => {
			this.setState({monthDays: getMonthDays(this.state.year, this.state.month), monthName: getMonthName(this.state.month)});
		});

	}

	render() {
		const { monthName, monthDays, year } = this.state;
		const { events, selectedDay, onMonthChange, shortWeekDay, selectDay } = this.props;

		return (
			<Card className="app-card">
				<CardHeader
					action={
						<div>
							<IconButton onClick={() => { this.changeMonth(-1); onMonthChange && onMonthChange(); }}>
								<ArrowLeft />
							</IconButton>
							<IconButton onClick={() => { this.changeMonth(1); onMonthChange && onMonthChange(); }}>
								<ArrowRight />
							</IconButton>
						</div>
					}
					title={`${monthName}, ${year}`}
				/>
				<CardContent>

					<table className="app-calendar-table">
						<tbody>
						<tr className="week-days-row">
							{
								WeekDays.map((weekDay, i) => (<th key={i} className="day">{ shortWeekDay ? weekDay.short : weekDay.full }</th>))
							}
						</tr>
						{
							monthDays.map((week, i) => (
								<tr key={i}>
									{
										week.map((day, i) => (
											<th key={i} className="day">
												{
													!!day ?
														<Button className={`day-button${day.dateString === this.currentDay ? ' current-day' : ''}`} disabled={selectedDay === day.dateString} onClick={() => { selectDay(day) }} >
															<Badge color="secondary" variant="dot" className="app-badge" invisible={!events[day.dateString]}>
																{day.dayNumber}
															</Badge>
														</Button> : <div></div>
												}
											</th>
										))
									}
								</tr>
							))
						}
						</tbody>
					</table>
				</CardContent>
			</Card>


		)
	}
}

export default Calendar;