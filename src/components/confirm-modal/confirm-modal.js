import React, { Component } from 'react';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import './confirm-modal.css';

class ConfirmModal extends Component {
	render() {
		const { title, question, open, onClose, confirm } = this.props;
		return (
			<Dialog
				open={open}
				onClose={onClose}
				aria-labelledby="alert-dialog-title"
				aria-describedby="alert-dialog-description">
				<DialogTitle id="alert-dialog-title">{ title }</DialogTitle>
				<DialogContent>
					<DialogContentText id="alert-dialog-description">
						{ question }
					</DialogContentText>
				</DialogContent>
				<DialogActions>
					<Button onClick={() => { onClose(); }} color="primary">
						No
					</Button>
					<Button onClick={() => { onClose(); confirm(); }} color="primary" autoFocus>
						Yes
					</Button>
				</DialogActions>
			</Dialog>
		)
	}
}
export default ConfirmModal;