/* eslint-disable import/first */
import React, { Component } from 'react';

import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect'

import { Router, Route, Redirect, Switch } from 'react-router-dom';
import createBrowserHistory from 'history/createBrowserHistory';

import { ProtectedRoute } from './utils/protected.router';

const history = createBrowserHistory();

// Screens
import EventsScreen from './containers/events-screen/events-screen';
import NotesScreen from './containers/notes-screen/notes-screen';
import SettingsScreen from './containers/settings-screen/settings-screen';
import HomePage from './containers/home-screen/home-screen'
import AuthScreen from './containers/auth-screen/auth-screen';
import NotFoundPage from './containers/not-found-page/not-found-page';

// Components
import Header from './components/header/header';

import './App.css';

// Selectors

// Actions
import { getUserProfile } from './redux/actions/auth';

class App extends Component {

	componentDidMount() {
		const { getUserProfile } = this.props;
		getUserProfile();
	}

	render() {
		return (
            <Router history={history}>

                <main className="app-container">
	                <Route path="/" component={Header} />
	                <Switch>
		                <Route path="/auth/:authType" component={AuthScreen} />


		                <ProtectedRoute path="/" component={HomePage} exact />

		                <ProtectedRoute path="/events" component={EventsScreen} />
		                <ProtectedRoute path="/notes" component={NotesScreen} />
		                <ProtectedRoute path="/settings" component={SettingsScreen} />

		                <Route path="" component={NotFoundPage} />
	                </Switch>
                </main>

            </Router>
		);
	}
}

const mapStateToProps = (state) => createStructuredSelector({

});
const mapDispatchToProps = dispatch => ({
	getUserProfile: () => dispatch(getUserProfile())
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
