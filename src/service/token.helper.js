class TokenHelper {
	_token;
	set token(token) {
		localStorage.setItem('token', token);
		this._token = !!token;
	}
	get token() {
		this._token = !!localStorage.getItem('token');
		return this._token;
	}
}
export default new TokenHelper();