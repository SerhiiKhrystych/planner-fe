import Http from '../utils/http';

// User
export const fetchLogin = (user) => Http.post('/users/login', user);
export const fetchRegister = (user) => Http.post('/users/register', user);
export const fetchUserProfile = (user) => Http.get('/users/profile');
export const fetchUserProfileUpdate = (user) => Http.put('/users/profile', user);


// Events
export const fetchEvents = () => Http.get('/events');
export const fetchEvent = (id) => Http.get(`/events/${id}`);
export const fetchEventNew = (event) => Http.post('/events', event);
export const fetchEventUpdate = (event, title, description, unread) => Http.put('/events', event);
export const fetchEventDelete = (id) => Http.remove(`/events/${id}`);

// Notes
export const fetchNotes = () => Http.get('/notes');
export const fetchNote = (id) => Http.get(`/notes/${id}`);
export const fetchNoteNew = (note) => Http.post('/notes', note);
export const fetchNoteUpdate = (id, note) => Http.put('/notes', { id, ...note });
export const fetchNoteDelete = (id) => Http.remove(`/notes/${id}`);


