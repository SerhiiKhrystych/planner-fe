import axios from 'axios';
import { API_URL } from '../utils/constants';

const headers = {
	'Content-Type': 'application/json'
};

class Http {
	http;
	token = localStorage.getItem('token') || null;

	constructor() {
		this.http = axios.create({
			baseURL: API_URL,
			defaultInterceptors: true,
		});
	}

	get(path, config) {
		const token = localStorage.getItem('token');
		if (!token) return Promise.reject();
		const reqHeaders =  {
			...headers,
			'Authorization': token
		};

		return axios.get(`${API_URL}${path}`, { ...config, headers: reqHeaders });
	}

	post(path, data) {
		const token = localStorage.getItem('token');
		const reqHeaders =  {
			...headers,
			'Authorization': token
		};

		return axios.post(`${API_URL}${path}`, data, { headers: reqHeaders });
	}

	put(path, data) {
		const token = localStorage.getItem('token');
		const reqHeaders =  {
			...headers,
			'Authorization': token
		};

		return axios.put(`${API_URL}${path}`, data, { headers: reqHeaders });
	}

	remove(path) {
		const token = localStorage.getItem('token');
		const reqHeaders =  {
			...headers,
			'Authorization': token
		};

		return axios.delete(`${API_URL}${path}`, { headers: reqHeaders });
	}
}

export default new Http();