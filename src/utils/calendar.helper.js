export const WeekDays = [
	{ full: 'Monday', short: 'Mod' },
	{ full: 'Tuesday', short: 'Tue' },
	{ full: 'Wednesday', short: 'Wed' },
	{ full: 'Thursday', short: 'Th' },
	{ full: 'Friday', short: 'Fri' },
	{ full: 'Saturday', short: 'Sat' },
	{ full: 'Sunday', short: 'Sun' }
];
export const Months = [
	'January',
	'February',
	'March',
	'April',
	'May',
	'June',
	'Jule',
	'August',
	'September',
	'October',
	'November',
	'December'
];

export const daysInMonth = (year, month) => {
	return new Date(year, month + 1, 0).getDate();
};

export const getMonthStartWeekDay = (year, month) => {
	return new Date(year, month, 1).getDay();
};

export const getFirstMondayOfMonthDate = (year, month, day = 1) => {
	if (new Date(year, month, day).getDay() === 1) {
		return day;
	} else {
		day++;
		return getFirstMondayOfMonthDate(year, month, day);
	}
};

export const getCurrentYear = () => {
	return new Date().getFullYear();
};

export const getCurrentMonth = () => {
	return new Date().getMonth();
};

export const getMonthName = (month) => {
	if (!month && month !== 0) month = getCurrentMonth();
	return Months[month];
};

export const getMonthDays = (year, month) => {
	if (!year) year = getCurrentYear();
	if (!month && month !== 0) month = getCurrentMonth();

	const firstMondayOfMonthDate = getFirstMondayOfMonthDate(year, month);
	const allDays = [];
	const firstWeek = [];
	const middleWeeks = [];
	const result = [];

	/**
	 * generate all weeks except first incomplete
	 */
	for (let i = firstMondayOfMonthDate, length = daysInMonth(year, month); i <= length; i++ ) {
		const date = new Date(`${year}-${month + 1}-${i}`);
		allDays.push({
			dayNumber: i,
			date: new Date(year, month, i),
			dateString: date.toDateString(),
		});
	}

	for (let i = 0, length = allDays.length; i < length; i+=7) {
		middleWeeks.push(allDays.slice(i, i + 7));
	}

	for (let i = 1, until = firstMondayOfMonthDate; i < until; i++ ) {
		const date = new Date(`${year}-${month + 1}-${i}`);
		firstWeek.push({
			dayNumber: i,
			date: new Date(year, month, i),
			dateString: date.toDateString(),
			dayId: `${date.getMonth()}-${date.getDate()}-${date.getFullYear()}`
		});
	}
	if (firstWeek.length > 0) {
		for (let i = 0, length = 7 - firstWeek.length; i < length; i++ ) {
			firstWeek.unshift(null);
		}
		result.push(firstWeek);
	}
	// no sense
	if (middleWeeks.length > 0) {
		result.push(...middleWeeks);
	}
	return result
};

export const getReadableDate = (date) => `${new Date(date).getDate()}th ${Months[new Date(date).getMonth()]}, ${new Date(date).getFullYear()}`;


export const generateDayId = (date) => `${date.getMonth()}-${date.getDate()}-${date.getFullYear()}`;

export const getCurrentDate = () => new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());