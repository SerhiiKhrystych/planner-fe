import * as Yup from 'yup';

import { equalTo } from './yup.custom.methods';
import { phoneNumberRegExp, passwordReqExp } from './reg-exps';

Yup.addMethod(Yup.string, 'equalTo', equalTo);

export const FORMS = {
	registration: {
		validationSchema: Yup.object().shape({
			firstName: Yup.string()
				.min(2, 'Too Short!')
				.max(50, 'Too Long!')
				.required('First Name Required'),
			lastName: Yup.string()
				.min(2, 'Too Short!')
				.max(50, 'Too Long!')
				.required('Last Name Required'),
			username: Yup.string()
				.min(2, 'Username is too short - should be 2 chars minimum.')
				.required('Username required'),
			email: Yup.string()
				.email('Invalid Email')
				.required('Email Required'),
			phoneNumber: Yup.string()
				.matches(phoneNumberRegExp, 'Phone Number is not valid')
				.required('Phone Number required'),
			password: Yup.string()
				.min(8, 'Password is too short - should be 8 chars minimum.')
				.matches(passwordReqExp, 'Password can only contain Latin letters.')
				.required('Password required.'),
			confirmPassword: Yup.string()
				.equalTo(Yup.ref('password'), 'Passwords must match')
				.required('Required'),
		}),
		formLabel: 'Registration',
		fields: [
			{
				label: 'First Name',
				name: 'firstName',
				placeholder: 'Enter first name',
				type: 'text',
			},
			{
				label: 'Last Name',
				name: 'lastName',
				placeholder: 'Enter last name',
				type: 'text',
			},
			{
				label: 'User Name',
				name: 'username',
				placeholder: 'Enter username',
				type: 'text',
			},
			{
				label: 'Email',
				name: 'email',
				placeholder: 'Enter email',
				type: 'email',
			},
			{
				label: 'Phone Number',
				name: 'phoneNumber',
				placeholder: 'Enter Phone Number',
				type: 'text'
			},
			{
				label: 'Password',
				name: 'password',
				placeholder: 'Enter password',
				type: 'password',
			},
			{
				label: 'Confirm Password',
				name: 'confirmPassword',
				placeholder: 'Confirm password',
				type: 'password',
			}
		],
		initialValues: {
			firstName: '',
			lastName: '',
			username: '',
			email: '',
			phoneNumber: '',
			password: '',
			confirmPassword: ''
		},
		submitButtonText: 'Submit'
	},
	login: {
		validationSchema: Yup.object().shape({
			username: Yup.string()
				.min(2, 'Username is too short - should be 2 chars minimum.')
				.required('Username required'),
			password: Yup.string()
				.min(8, 'Password is too short - should be 8 chars minimum.')
				.matches(passwordReqExp, 'Password can only contain Latin letters.')
				.required('Password required.')
		}),
		formLabel: 'Login',
		fields: [
			{
				label: 'Username',
				name: 'username',
				placeholder: 'Enter Username',
				type: 'text',
			},
			{
				label: 'Password',
				name: 'password',
				placeholder: 'Enter password',
				type: 'password',
			},
		],
		initialValues: {
			username: '',
			password: '',
		},
		submitButtonText: 'Submit',
	},
	'forgot-password': {
		validationSchema: Yup.object().shape({
			email: Yup.string()
				.email('Invalid email')
				.required('Email required')
		}),
		formLabel: 'Forgot password?',
		fields: [
			{
				label: 'Email',
				name: 'email',
				placeholder: 'Enter your email here',
				type: 'email',
			},
		],
		initialValues: {
			email: ''
		},
		submitButtonText: 'Send'
	},
	settings: {
		validationSchema: Yup.object().shape({
			firstName: Yup.string()
				.min(2, 'Too Short!')
				.max(50, 'Too Long!')
				.required('First Name Required')
				.default('1488'),
			lastName: Yup.string()
				.min(2, 'Too Short!')
				.max(50, 'Too Long!')
				.required('Last Name Required'),
			email: Yup.string()
				.email('Invalid email')
				.required('Email required'),
		}),
		formLabel: 'Settings',
		fields: [
			{
				label: 'First Name',
				name: 'firstName',
				placeholder: 'Enter First Name',
				type: 'text',
			},
			{
				label: 'Last Name',
				name: 'lastName',
				placeholder: 'Enter Last Name',
				type: 'text',
			},
			{
				label: 'Email',
				name: 'email',
				placeholder: 'Enter Email',
				type: 'email',
			}
		],
		submitButtonText: 'Submit'
	},
	note: {
		validationSchema: Yup.object().shape({
			title: Yup.string()
				.min(2, 'Too Short!')
				.max(16, 'Too Long!')
				.required('Note Title Required')
				.default(''),
			priority: Yup.string()
				.default('Middle'),
			description: Yup.string()
				.min(2, 'Too Short!')
				.max(1000, 'Too Long!')
				.required('Note Description Required')
				.default(''),
		}),
		formLabel: 'Note',
		fields: [
			{
				label: 'Title',
				name: 'title',
				placeholder: 'Enter Title',
				type: 'text',
				rows: 1
			},
			{
				label: 'Priority',
				name: 'priority',
				type: 'radio',
				options: [
					'Low',
					'Middle',
					'High'
				]
			},
			{
				label: 'Description',
				name: 'description',
				placeholder: 'Enter Description',
				type: 'text',
			}
		],
		cancelButtonText: 'Cancel',
		submitButtonText: 'Save'
	},
	event: {
		validationSchema: Yup.object().shape({
			title: Yup.string()
				.min(2, 'Too Short!')
				.max(16, 'Too Long!')
				.required('Event Title Required')
				.default(''),
			date: Yup.string()
				.required('Event Date Required'),
			description: Yup.string()
				.min(2, 'Too Short!')
				.max(1000, 'Too Long!')
				.required('Event Description Required')
				.default('')
		}),
		formLabel: 'Event',
		fields: [
			{
				label: 'Title',
				name: 'title',
				placeholder: 'Enter Title',
				type: 'text',
				rows: 1
			},
			{
				label: 'Date',
				name: 'date',
				placeholder: 'Enter Date',
				type: 'date'
			},
			{
				label: 'Description',
				name: 'description',
				placeholder: 'Enter Description',
				type: 'text',
			}
		],
		cancelButtonText: 'Cancel',
		submitButtonText: 'Save'
	}
};