export const READ_NOTE = '[Note] Read Note';
export const CREATE_NOTE = '[Note] Create Note';
export const UPDATE_NOTE = '[Note] Update Note';

export const READ_EVENT = '[Event] Read Event';
export const CREATE_EVENT = '[Event] Create Event';
export const UPDATE_EVENT = '[Event] Update Event';

export const API_URL = 'http://localhost:2000';