import io from 'socket.io-client';
import { API_URL } from '../utils/constants';

class Socket {
	io;
	token = localStorage.getItem('token') || null;

	constructor() {
		this.io = io(API_URL, { query: { token: this.token } });
	}

	on(connectionType, event) {
		this.io.on('push', ({ type, payload }) => {
			if (type === connectionType) {
				event();
			}
		});
	}

	disconnect() {
		this.io.disconnect();
	}
}

export default Socket;