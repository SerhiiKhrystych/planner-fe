import React from 'react';
import { Route, Redirect } from 'react-router-dom';

import Auth from './auth';

export const ProtectedRoute = ({ component: Component, ...rest }) => (
	<Route {...rest} render={(props) => (
		Auth.getToken() !== null ? (
			<Component {...props} />
		) : (
			rest.children
		)
	)} />
);