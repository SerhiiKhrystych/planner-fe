import * as Yup from 'yup';

export const equalTo = (ref, msg) => {
	return Yup.mixed().test({
		name: 'equalTo',
		exclusive: false,
		message: msg || '${path} must be the same as ${reference}',
		params: {
			reference: ref.path,
		},
		test: function(value) {
			return value === this.resolve(ref);
		},
	});
};